# Téléchargement par lot sur Nakala

## Présentation

*Dernière MàJ : 13/02/2025*

[Nakala](https://nakala.fr) est un entrepôt de données de recherche pour les Sciences Humaines et Sociales permettant de déposer les données de recherche issues de projets scientifiques du monde académique français ([Utiliser Nakala](https://documentation.huma-num.fr/nakala/)).

Le script python [script_telechargement_nakala.py](script_telechargement_nakala.py) présent dans ce dépôt permet de télécharger les [données](https://documentation.huma-num.fr/nakala/#une-donnee-dans-nakala) (fichiers + métadonnées) regroupées au sein d'une collection Nakala en utilisant l'[API de Nakala](https://api.nakala.fr/doc).

En fonction du [statut](https://documentation.huma-num.fr/nakala/#statut-prive-ou-public) de la collection et des données qu'elle contient, il est possible de télécharger des données sans posséder de compte Nakala.

##  1. Prérequis à l'utilisation du script

### 1.1. Python et librairies

Pour utiliser ce script, il est nécessaire d'avoir installé Python sur votre ordinateur. Nous vous recommandons d'utiliser [Python 3](https://www.python.org/downloads/) car le script a été créé avec cette version, l'utilisation d'une version antérieure est susceptible de provoquer des erreurs dans le code.

Les deux librairies Python suivantes doivent être installées avant l'exécution du script :
- **requests** (saisissez dans votre terminal :  ```python3 -m pip install requests```)
- **tqdm** (saisissez dans votre terminal :  ```python3 -m pip install tqdm```)

### 1.2. Droits et compte Nakala


#### 1.2.1. Statut public et fichiers sans embargo
Si la collection est au [statut public](https://documentation.huma-num.fr/nakala/#statut-prive-ou-public) et que les fichiers des données qu'elle regroupe ne sont pas sous [embargo](https://documentation.huma-num.fr/nakala/#acces-restreint-sur-un-fichier-embargo), vous n'avez pas besoin de compte Nakala pour télécharger les données (fichiers + métadonnées).

*Remarque* : Si à partir d'une collection publique vous souhaitez télécharger des données avec des fichiers sous embargo sans posséder de compte Nakala, vous pourrez télécharger les métadonnées mais pas les fichiers.

#### 1.2.2. Statut privé ou fichiers sous embargo
Dans les autres cas (collection et/ou données au statut privé, données au statut public avec des fichiers sous embargo), il est nécessaire dans un premier temps de posséder [un compte Nakala](https://documentation.huma-num.fr/nakala/#liens-utiles).

Dans un second temps, vous devez posséder des [droits](https://documentation.huma-num.fr/nakala/#gestion-des-droits-sur-une-ressource) sur ces données et/ou cette collection. 
<br>Les droits minimums suffisent, vous devez donc avoir au moins le rôle de lecteur (cf. [la liste des rôles](https://documentation.huma-num.fr/nakala/#liste-des-roles) pour les droits).

**/!\ Attention**, avoir des droits sur une collection ne signifie pas que vous possédez automatiquement les mêmes droits pour les données qu'elle contient :

> Les droits de gestion d’une collection sont indépendants des droits de gestion sur les données qu’elle regroupe. Cela signifie que si vous donnez les droits de lecture à un utilisateur sur une collection qui contient des données non publiées, l’utilisateur n’aura pas automatiquement les droits de lecture sur ces données. Vous devrez attribuer à ce même utilisateur des droits de lecture au niveau de chaque donnée si vous souhaitez qu’il puisse y avoir accès.
>
> — <cite>[Documentation de l’entrepôt Nakala](https://documentation.huma-num.fr/nakala/#gestion-des-droits-sur-une-ressource)</cite>

*Remarque* : Si à partir d'une collection publique (ou privée pour laquelle vous avez des droits) vous souhaitez télécharger des données publiques avec des fichiers sous embargo pour lesquelles vous n'avez pas de droits, vous pourrez télécharger les métadonnées mais pas les fichiers.

## 2. Utilisation du script

### 2.1. Compléter le script

#### 2.1.1. Variables à compléter 

Pour utiliser le script, il faut obligatoirement saisir l'**identifiant Nakala de la collection** (son **ID**, présent sur la page de la collection) dans la variable ```id_collection```. Veillez à ne pas ajouter d'espaces lors de la saisie de la variable.

<img src="Illustrations/ID_collection.png" alt="Image montrant où se trouve le DOI d'une collection dans Nakala" width="500" />

Si des droits sont nécessaires pour télécharger les données de cette collection (cf. partie précédente), vous devrez saisir votre **clé d'API** (présente dans le profil de votre compte Nakala) dans la variable ```api_clef```. Veillez à ne pas ajouter d'espaces lors de la saisie de la variable. 
<br>Sinon, veuillez laisser vide la variable ```api_clef```. 

<img src="Illustrations/clef_api.png" alt="Image montrant où se trouve la clé d'API d'une utilisateur dans Nakala" width="600" />

#### 2.1.2. Variables à modifier (facultatif) 

Les variables suivantes peuvent être modifiées si vous le souhaitez :

* ```prefixe``` : préfixe à ajouter au début du nom de chaque fichier téléchargé (ex : avec ```prefixe = 'AcronymeProjet_'```, les fichiers déposés dans Nakala aux noms "photo1_Rennes_2024.png" et "photo2_Rennes_2024.png" seront téléchargés aux noms "AcronymeProjet_photo1_Rennes_2024.png" et "AcronymeProjet_photo2_Rennes_2024.png")
* ```dossier``` : chemin relatif du dossier qui contiendra les fichiers téléchargés, par défaut dans un dossier nommé 'collection_files'
* ```outfile_json``` : nom du fichier JSON de sortie contenant les métadonnées des données téléchargées, par défaut 'datas_in_collection.json' (veillez à ne pas modifier l'extension du fichier)
* ```outfile_csv``` : nom du fichier CSV de sortie contenant l'état des téléchargements des fichiers, par défaut 'files_downloaded.csv' (veillez à ne pas modifier l'extension du fichier)
* ```encoding``` : encodage utilisé dans les fichiers de sortie, par défaut 'utf-8'
* ```temp``` : temps (en secondes) de temporisation avant chaque téléchargement de fichier, par défaut '0.5' (à modifier en fonction du poids des fichiers à télécharger)

### 2.2. Sorties obtenues

Une fois le script complété et exécuté, les trois sorties suivantes sont obtenues (dans le dossier où vous avez exécuté le script)  :
1. un fichier JSON contenant les **métadonnées** des données téléchargées (nom du fichier JSON dans la variable ```outfile_json```),
2. un dossier contenant les **fichiers** des données téléchargées (nom du dossier dans la variable ```dossier```),
3. un fichier CSV contenant des **informations** pour chaque fichier téléchargé (nom du fichier CSV dans la variable ```outfile_csv```), possédant l'en-tête suivante :
    - *filename* : nom du fichier
    - *sha1* : sha1 du fichier
    - *data ID* : identifiant Nakala de la donnée à laquelle appartient le fichier
    - *status* : statut du téléchargement du fichier, contient une des valeurs suivantes :
        - 'OK' : le fichier a été téléchargé
        - 'ERROR' : le fichier n'a pas pu être téléchargé
    - *code* : code de réponse de l'API suite à la requête de téléchargement du fichier, contient par exemple :
        - '201' : code indiquant que le téléchargement du fichier a eu lieu
        - '401' : code indiquant que le téléchargement du fichier est refusé par manque de droit nécessaire sur la donnée 
    - *message* : message expliquant le succès du téléchargement ou les raisons de son échec (message de réponse de l'API suite à la requête de téléchargement du fichier )

### 2.3. Messages d'erreur possibles

Lors de la tentative d'accès via l'API à la collection dont les données qu'elle regroupe seront téléchargées, trois types d'erreur peuvent arriver (l'erreur sera affichée dans la console) : 
* Erreur 401 : Collection privée, nécessite une clé d'API et les droits sur la collection
* Erreur 403 : Collection privée, nécessite les droits dessus
* Erreur 404 : Aucune collection n'existe pour cet identifiant

Si un de ces messages d'erreur apparait, veuillez vérifier si : 
* l'identifiant de la collection a été correctement saisi (sans espace)
* la collection existe
* votre clé d'API a été correctement saisie (sans espace) si votre clé d'API est nécessaire
* vous avez des droits sur la collection si elle est privée

## Conclusion

L'équipe de la Plateforme Humanités Numériques espère que ce script vous aura permis de télécharger les données voulues sur Nakala.

Si vous êtes rattaché•e à une unité de recherche en SHS située en Bretagne et que vous souhaitez un accompagnement pour télécharger des données par lot, veuillez envoyer un mail à numerique@mshb.fr.

Si vous êtes rattaché•e à une unité de recherche en SHS située dans une autre région et que vous souhaitez un accompagnement, veuillez contacter [la MSH la plus proche](https://www.huma-num.fr/carte-des-relais-huma-num-dans-les-msh/).

<br>*Plateforme Humanités Numériques* – [MSHB](https://www.mshb.fr) <br> 
📧 numerique@mshb.fr

<img src="Illustrations/Logo_MSHB_rouge.jpeg" width="200"/>
