"""
- description : Téléchargement de données par lot sur Nakala à partir d'une collection
- author : Chloé Choquet (MSHB - Plateforme Humanités Numériques)
- license : CC BY-NC-SA
- date : 2023-04
- update : 2024-05-27
"""

# Variables à compléter
id_collection = "" # Identifiant Nakala de la collection (ID, présent sur la page de la collection) contenant les données à télécharger 
api_clef = "" # clé d'API (présente dans votre profil nakala), à renseigner seulement si des données sont privées ou si des fichiers sont sous embargo (nécessaire dans ce cas d'avoir des droits sur ces données)

# Variables à modifier (facultatif)
prefixe = ""
dossier = "./collection_files/"+prefixe # chemin relatif du dossier qui contiendra les fichiers téléchargés
outfile_json = "datas_in_collection.json" # nom du fichier JSON de sortie contenant les métadonnées des données téléchargées
outfile_csv = "files_downloaded.csv" # nom du fichier CSV de sortie contenant l'état des téléchargements des fichiers 
encoding = "utf-8" # encodage utilisé dans les fichiers de sortie
temp = 0.5 # temps (en secondes) de temporisation avant chaque téléchargement de fichier

# Librairies
import csv, json, os, requests, time
from tqdm import tqdm

# Variables 
api_url = "https://api.nakala.fr"
cpt_error = 0 # compteur d'erreur pour les fichiers non téléchargés
cpt_files = 0 # nombre total de fichiers à télécharger
headers = {'Content-Type': 'application/json'}
if api_clef != "" :
   headers['X-API-KEY'] = api_clef

# Début du programme 
print("\n## PROGRAMME EN COURS : Téléchargement par lot sur Nakala à partir de la collection '"+id_collection+"'\n")

# Accès à la collection
url_requete =  'https://api.nakala.fr/collections/' + id_collection + '/datas?page=1&limit=25'
response_collection = requests.request('GET', url_requete, headers=headers)
if response_collection.status_code == 200 : # on obtient le code 200 si l'ID de la collection entré existe et qu'il n'y a pas de problème de droits
   
   # Récupération du nombre de pages (les métadonnées des données de la collection sont affichées sur plusieurs pages)
   nb_pages = response_collection.json()['lastPage']

   # Création du dossier qui contiendra les fichiers téléchargés
   try:
      os.makedirs(dossier)
   except FileExistsError: # si le dossier existe déjà 
      pass
   
   # Préparation du fichier CSV de sortie 
   output_csv = open(outfile_csv, 'w', encoding=encoding) 
   outputWriter = csv.writer(output_csv) # création d'un objet pour écrire dans ce fichier
   header_csv = ['filename', 'sha1', 'data ID', 'status', 'code', 'message'] # nom des colonnes à insérer dans ce fichier
   outputWriter.writerow(header_csv) # écriture du nom des colonnes dans ce fichier

   # Récupération des métadonnées et téléchargement des fichiers
   liste_metadatas = []
   for i in tqdm(range(1,nb_pages+1), desc="Téléchargement des fichiers et récupération des métadonnées : ") :
      url_requete = api_url + './collections/' + id_collection + '/datas?page='+str(i)+'&limit=25'
      response = requests.request('GET', url_requete, headers=headers) 

      # Récupération des métadonnées
      page_datas = response.json()['data'] 
      for data in page_datas :
         liste_metadatas.append(data) 

         # Téléchargement des fichiers
         id_data = data['identifier']
         for file in data['files'] :
               time.sleep(temp) # temporisation avant chaque téléchargement de fichier
               cpt_files += 1
               filename = file['name']
               sha1 = file['sha1']
               url = api_url + '/data/' + id_data + '/' + sha1
               r = requests.get(url, allow_redirects=True) # requête pour télécharger le fichier
               outputData = [filename, sha1, id_data, '', r.status_code, ''] # informations pour le fichier de sortie CSV
               if r.status_code == 200 : # on obtient le code 200 si le téléchargement du fichier est possible
                  outputData[3] = 'OK'
                  outputData[5] = 'Fichier téléchargé'
                  open(dossier+filename, 'wb').write(r.content)
               else : 
                  outputData[3] = 'ERROR'
                  outputData[5] = r.text
                  cpt_error += 1
               outputWriter.writerow(outputData) # Écriture de l'état du téléchargement dans le fichier de sortie CSV
   
   # Fermeture du fichier CSV de sortie
   output_csv.close()

   # Écriture de la liste des métadonnées dans le fichier de sortie JSON
   with open(outfile_json, "w", encoding=encoding) as outfile :
      json.dump(liste_metadatas, outfile, indent=4, ensure_ascii=False)

elif response_collection.status_code == 401 :
   print("/!\\ ERREUR 401 : Collection privée, nécessite une clé d'API et les droits sur la collection /!\\")

elif response_collection.status_code == 403 :
   print("/!\\ ERREUR 403 : Collection privée, nécessite les droits dessus /!\\")

elif response_collection.status_code == 404 :
   print("/!\\ ERREUR 404 : Aucune collection n'existe pour cet identifiant /!\\")

else :
   print("/!\\ ERREUR : Erreur inconnue lors de la tentative d'accès à la collection /!\\")

# Fin du programme
if response_collection.status_code == 200 :
   print("\n-> Les métadonnées ont été téléchargées dans le fichier de sortie '"+outfile_json+"'")
   print("\n-> "+str(cpt_files-cpt_error)+"/"+str(cpt_files)+" fichiers ont été téléchargés dans le dossier '"+dossier+"'")
   print("\n-> Pour plus d'informations sur les fichiers téléchargés, veuillez consulter le fichier de sortie '"+outfile_csv+"'")
print("\n# # # # # # # # # # # \n# FIN DU PROGRAMME  #\n# # # # # # # # # # # \n")
